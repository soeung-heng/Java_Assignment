import java.util.*;
import java.io.*;

public class employee{

	public String id;
	public String lastName;
	public String firstName;
	public int age;
	public String position;
	public double salary;
	public String address;
	public String email;

	Console console = System.console();
	Scanner input = new Scanner(System.in);
	



	// public enum posi{
	// 	public String doctor,nurse,cleaner,manager;
	// }

	public String getemp(){
		String result = id+"\t"+lastName+"\t"+firstName+"\t"+age+"\t"+position+"\t"+salary+"\t"+address+"\t"+email;
		return result;
	}

	public void setemp(){
		try{
		
		System.out.print("\t\t ID : ");
		id = input.next();

		int exist = 0;
		String scr;
		String[] item;
		FileReader fr = new FileReader("empData.dat");
		BufferedReader br = new BufferedReader(fr);
		while((scr = br.readLine()) != null){

			item = (scr.split("\t"));

			if(item[0].equals (id))
			{
				exist = 1;
			}
			item = null;
		}
		br.close();

		if (exist == 1) {
			System.out.print("Employee id is already have. Please input another ID.");
			setemp();
			
		}else{

			System.out.print("\t\t LastName : ");
			lastName = input.next();
			System.out.print("\t\t FirstName : ");
			firstName = input.next();
			System.out.print("\t\t Age : ");
			age = input.nextInt();
			if(age<18){
					System.out.println("You've enter an invalid employee age..!");
					System.out.print("\t\t Age : ");
					age = input.nextInt();
			}
			System.out.print("\t\t Position : ");
			position = input.next();
			System.out.print("\t\t Salary : ");
			salary = input.nextDouble();
			System.out.print("\t\t Email : ");
			email = input.next();
			System.out.print("\t\t Address : ");
			address = input.next();
		}
		
		File empData = new File("empData.dat");
		if(!empData.exists()){
			empData.createNewFile();
			System.out.print("New File Created..!");
		}

		FileWriter fw = new FileWriter(empData, true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(getemp());
		bw.newLine();
		System.out.print("\n\tRecord Saved..!");
		bw.close();

	}catch(IOException e){
		e.printStackTrace();
	}

	}
	public void modifyEmp(){

		String showitem = "";
		String empName = "";
		System.out.println("\n\t\t\t\t\t\t*UPDATE* \n\n");
		System.out.print("\tPlease enter Employee ID you want to update: ");
		String idInput = "";
		idInput = input.next();
		try 
		{
			//Read String From File
			FileReader fr = new FileReader("empData.dat");
			BufferedReader in = new BufferedReader(fr);
			String str;
			String[] sp = null;
			while ((str = in.readLine()) != null)
			{
				sp = (str.split("\t"));	
				if(sp[0].equals (idInput))
				{
					showitem  = str;
					empName = sp[1];
				}
				sp = null;
			}
			in.close();
			if (showitem.length()>5)
			{
				System.out.println("\n\t\t"+showitem);

				System.out.println("\t\tID : "+idInput);
				System.out.println("\t\tLastName : "+empName);

				System.out.println("\n\t\t- ENTER NEW DATA\n");
				lastName = empName;
				id = idInput;

				System.out.print("\t\tLastName : ");
				lastName = input.next();

				System.out.print("\t\tFirstName : ");
				firstName = input.next();

				System.out.print("\t\tAge : ");
				age = input.nextInt();
				while(age <18){

					System.out.println("\t\t\t\t\tYou've enter an invalid employee age..!");
					System.out.print("\t\tAge : ");
					age = input.nextInt();
				}

				System.out.print("\t\tPosition : ");
				position = input.next();

				System.out.print("\t\tSalary : ");
				salary = input.nextDouble();
				while(salary<=0){
					System.out.println("\t\t\t\t\tYou've input an invalid number of salary.\n\t\t\t\t\t\tPlease Input New Salary !");
					System.out.print("\t\tSalary : ");
					salary = input.nextDouble();
				}

				System.out.print("\t\tAddress : ");
				address = input.next();

				System.out.print("\t\tEmail : ");
				email = input.next();

				System.out.println("\n\t\t\t\t\t\t*NEW DATA*\n");
				System.out.println("\t\t"+getemp());

				System.out.print("\nAre you sure want to modify this record? (Y/N): ");
				String YesNo = input.next().toUpperCase();
				System.out.println(YesNo);

				switch(YesNo)
						{
							case "Y": 
								 try 
								 {
									File inputFile = new File("empData.dat");
									if (!inputFile.isFile()) {
										System.out.println("Parameter is not an existing file");
										return;
									}
									//Construct the new file that will later be renamed to the original filename.
									File tempFile = new File(inputFile.getAbsolutePath() + ".dat");
									BufferedReader br = new BufferedReader(new FileReader("empData.dat"));
									BufferedWriter pw = new BufferedWriter(new FileWriter(tempFile));
									String line = null;
 
									//Read from the original file and write to the new
									//unless content matches data to be removed.
									while ((line = br.readLine()) != null) 
									{
										if(line.equals (showitem))
										{
											
										}
										else
										{
											pw.write(line);
											pw.newLine();
										}
									}
									pw.close();
									br.close();
 
									//Delete the original file
									if (!inputFile.delete()) {
										System.out.print("Could not delete file.    ");
										return;
									}
 
									//Rename the new file to the filename the original file had.
									if (!tempFile.renameTo(inputFile))
									{
										System.out.print("Could not rename file.    ");
									}
								}
								catch (FileNotFoundException ex) 
								{
									ex.printStackTrace();
								}
								catch (IOException ex) 
								{
									ex.printStackTrace();
								}
								try 
								{
									File file = new File("empData.dat");
									FileWriter fw = new FileWriter(file, true);
        							BufferedWriter bw = new BufferedWriter(fw);
        							bw.write(getemp());
									bw.newLine();
        							System.out.print("\n\t\t\t\t\tRecord has been modified.    ");
									bw.close();		
									console.readLine();
								}
								catch (IOException e) 
								{
        							e.printStackTrace();
    							}
    						}
    				case "N":
							System.out.println("NO");	
						}else
						{
							System.out.print("\n\tThere are no Student Id " + idInput + " Record!!! Cannot use delete operation.    ");
						}
				}
				catch (IOException e) 
				{
        			e.printStackTrace();
    			}	 	

			}
		}catch (IOException e){
			e.printStackTrace();
		}
	}

}